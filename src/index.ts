import { Express } from 'express'
import { WinterPlugin, Environment, MongoDbConfig } from 'winter-core';
import { Server } from 'winter-core/dist/server';
import { EventEmitter } from 'events';
import { MongoClient, Db, Collection } from 'mongodb'

export class MongoDbPlugin extends EventEmitter implements WinterPlugin {
  private get DB_CONNECTED_EVENT(): string { return 'db-connected' }
  private get DB_CONNECTION_FAIL_EVENT(): string { return 'db-connection-fail' }
  config:MongoDbConfig

  private constructor() {
    super()
  }

  private static _instance: MongoDbPlugin
  static getInstance(): MongoDbPlugin {
    if (!MongoDbPlugin._instance) {
      MongoDbPlugin._instance = new MongoDbPlugin()
    }
    return this._instance
  }

  private _db: Db

  get db(): Db {
    return this._db
  }

  collection<T>(collection:string):Collection<T> {
    return this.db.collection<T>(collection)
  }

  apply(app: Express, environment:Environment, express) {
    
    if(environment){
      this.config = environment.database.mongodb
    }
    
    const { host, port, authDb, connectionDb } = this.config
    const url = `mongodb://${host}:${port}/${authDb}`

    MongoClient
      .connect(url, { useNewUrlParser: true })
      .then((mongoClient: MongoClient) => {
        console.info(`mongo connected to ${url}`)
        this._db = mongoClient.db(connectionDb)

        this.emit(this.DB_CONNECTED_EVENT, this._db)
      })
      .catch(error => {
        console.error(`mongo connection failed: ${error}`)
        Server.addError({ error })
        this.emit(this.DB_CONNECTION_FAIL_EVENT, error)
      })
  }

  onDbConnected(listener: (dbConfig: MongoDbPlugin) => void): MongoDbPlugin {
    this.addListener(this.DB_CONNECTED_EVENT, listener)
    return this
  }

  onDbConnectionFail(listener: (...args: any[]) => void): MongoDbPlugin {
    this.addListener(this.DB_CONNECTION_FAIL_EVENT, listener)
    return this
  }

}
